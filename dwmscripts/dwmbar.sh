#!/usr/bin/env bash

#while true; do
#
#	$HOME/dwmscripts/dwmupdates.sh > /home/shatterstone/dwmscripts/dwmupdates.txt
#
#	sleep 3600s
#
#done &
#printf "$(pwd)"
# printf "$HOME"
while true; do
	# BAR=$(xmobar -TSwaybar /home/shatterstone/.config/xmobar/xmobarrc-sway)
	CPU=$($HOME/dwmscripts/dwmcpu.sh)
	MEM=$($HOME/dwmscripts/dwmmemory.sh)
	#NET=$(dwmnet)
	# TEMP=$($HOME/dwmscripts/dwmtemp.sh)
	# HDD=$($HOME/dwmscripts/dwmdisk.sh)
	#UPDATES=$(< /home/steve/dwmscripts/dwmupdates.txt)
	BATT=$($HOME/dwmscripts/dwmbatt.sh)
	TIME=$($HOME/dwmscripts/dwmclock.sh)
	# xsetroot -name "$BAR"
	# xsetroot -name "| $CPU | $MEM | $TEMP | $HDD | $BATT | $TIME"
	xsetroot -name " $CPU | $MEM | $BATT | $TIME"
	# printf "running"

	sleep 1s
	
done &
