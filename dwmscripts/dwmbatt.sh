#!/usr/bin/env bash

STATUS=$(cat /sys/class/power_supply/BAT1/status)
BATT=$(cat /sys/class/power_supply/BAT1/capacity)

if [ "$STATUS" = "Discharging" ]; then

    ICON="🔋"
    ICON=""
else
    #ICON="⚡"
    ICON="Charging"
fi
icon2="Battery: "
#echo " $ICON" "$BATT%"
echo " $icon2 " "$BATT%" "$ICON"
